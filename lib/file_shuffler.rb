def shuffle_file
  print "Enter the file you want to shuffle: "
  input_name = gets.chomp

  old_file = File.read(input_name)

  shuffled_lines = old_file.split("\n").shuffle #[line_0, line_1, ...]

  new_file = File.open("#{input.gsub(/\.txt/, '')}-shuffled.txt", "w")

  shuffled_lines.each do |line|
    new_file.puts line
  end

  new_file.close
end


if __FILE__ == $PROGRAM_NAME
  shuffle_file
end
